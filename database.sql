CREATE TABLE Cases
(
    Id     INTEGER PRIMARY KEY AUTOINCREMENT,
    Number INTEGER,
    Type   TEXT,
    Date   TEXT
);

INSERT INTO Cases (Number, Type, Date)
VALUES (1, 'confirmed', '02-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (2, 'confirmed', '05-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (3, 'confirmed', '10-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (6, 'confirmed', '12-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1, 'deaths', '12-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (7, 'confirmed', '13-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (18, 'confirmed', '14-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1, 'recovered', '14-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (37, 'confirmed', '16-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (44, 'confirmed', '17-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2, 'deaths', '17-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (54, 'confirmed', '18-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (63, 'confirmed', '19-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2, 'recovered', '19-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (3, 'deaths', '20-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (3, 'recovered', '21-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (96, 'confirmed', '21-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (4, 'deaths', '22-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (115, 'confirmed', '22-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (5, 'recovered', '23-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (143, 'confirmed', '23-03-2020');


INSERT INTO Cases (Number, Type, Date)
VALUES (5, 'deaths', '24-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6, 'recovered', '24-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (170, 'confirmed', '24-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (6, 'deaths', '25-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7, 'recovered', '25-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (225, 'confirmed', '25-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (10, 'deaths', '26-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8, 'recovered', '26-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (275, 'confirmed', '26-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (23, 'deaths', '27-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11, 'recovered', '27-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (345, 'confirmed', '27-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (25, 'deaths', '28-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12, 'recovered', '28-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (402, 'confirmed', '28-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (26, 'deaths', '29-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13, 'recovered', '29-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (479, 'confirmed', '29-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (33, 'deaths', '30-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15, 'recovered', '30-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (556, 'confirmed', '30-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (36, 'deaths', '31-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (24, 'recovered', '31-03-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (602, 'confirmed', '31-03-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (39, 'deaths', '01-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (29, 'recovered', '01-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (654, 'confirmed', '01-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (44, 'deaths', '02-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (31, 'recovered', '02-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (708, 'confirmed', '02-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (48, 'deaths', '03-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (57, 'recovered', '03-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (791, 'confirmed', '03-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (59, 'deaths', '04-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (66, 'recovered', '04-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (919, 'confirmed', '04-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (70, 'deaths', '05-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (76, 'recovered', '05-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1021, 'confirmed', '05-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (80, 'deaths', '06-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (81, 'recovered', '06-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1120, 'confirmed', '06-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (90, 'deaths', '07-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (93, 'recovered', '07-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1184, 'confirmed', '07-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (93, 'deaths', '08-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (97, 'recovered', '08-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1275, 'confirmed', '08-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (96, 'deaths', '09-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (103, 'recovered', '09-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1346, 'confirmed', '09-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (107, 'deaths', '10-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (122, 'recovered', '10-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1448, 'confirmed', '10-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (111, 'deaths', '11-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (146, 'recovered', '11-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1545, 'confirmed', '11-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (118, 'deaths', '12-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (177, 'recovered', '12-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1661, 'confirmed', '12-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (126, 'deaths', '13-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (203, 'recovered', '13-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1763, 'confirmed', '13-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (126, 'deaths', '14-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (217, 'recovered', '14-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1888, 'confirmed', '14-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (127, 'deaths', '15-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (229, 'recovered', '15-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2024, 'confirmed', '15-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (130, 'deaths', '16-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (249, 'recovered', '16-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2283, 'confirmed', '16-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (135, 'deaths', '17-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (281, 'recovered', '17-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2564, 'confirmed', '17-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (137, 'deaths', '18-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (314, 'recovered', '18-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2685, 'confirmed', '18-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (141, 'deaths', '19-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (327, 'recovered', '19-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2855, 'confirmed', '19-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (143, 'deaths', '20-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (350, 'recovered', '20-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3046, 'confirmed', '20-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (145, 'deaths', '21-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (393, 'recovered', '21-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3209, 'confirmed', '21-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (149, 'deaths', '22-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (417, 'recovered', '22-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3446, 'confirmed', '22-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (155, 'deaths', '23-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (456, 'recovered', '23-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3568, 'confirmed', '23-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (158, 'deaths', '24-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (486, 'recovered', '24-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3758, 'confirmed', '24-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (159, 'deaths', '25-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (537, 'recovered', '25-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3897, 'confirmed', '25-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (161, 'deaths', '26-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (593, 'recovered', '26-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4065, 'confirmed', '26-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (162, 'deaths', '27-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (695, 'recovered', '27-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4120, 'confirmed', '27-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (165, 'deaths', '28-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (778, 'recovered', '28-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4252, 'confirmed', '28-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (168, 'deaths', '29-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (928, 'recovered', '29-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4321, 'confirmed', '29-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (170, 'deaths', '30-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (984, 'recovered', '30-04-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4423, 'confirmed', '30-04-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (171, 'deaths', '01-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1083, 'recovered', '01-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4569, 'confirmed', '01-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (173, 'deaths', '02-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1256, 'recovered', '02-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4729, 'confirmed', '02-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (174, 'deaths', '03-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1438, 'recovered', '03-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4903, 'confirmed', '03-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (179, 'deaths', '04-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1653, 'recovered', '04-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5053, 'confirmed', '04-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (181, 'deaths', '05-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (1838, 'recovered', '05-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5219, 'confirmed', '05-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (183, 'deaths', '06-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2017, 'recovered', '06-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5408, 'confirmed', '06-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (183, 'deaths', '07-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2179, 'recovered', '07-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5548, 'confirmed', '07-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (186, 'deaths', '08-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2324, 'recovered', '08-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5711, 'confirmed', '08-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (186, 'deaths', '09-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2461, 'recovered', '09-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5910, 'confirmed', '09-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (188, 'deaths', '10-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2554, 'recovered', '10-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6063, 'confirmed', '10-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (188, 'deaths', '11-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2811, 'recovered', '11-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6281, 'confirmed', '11-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (188, 'deaths', '12-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (2991, 'recovered', '12-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6418, 'confirmed', '12-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (188, 'deaths', '13-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3131, 'recovered', '13-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6512, 'confirmed', '13-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (190, 'deaths', '14-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3310, 'recovered', '14-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6607, 'confirmed', '14-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (190, 'deaths', '15-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3400, 'recovered', '15-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6652, 'confirmed', '15-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (192, 'deaths', '16-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3487, 'recovered', '16-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6741, 'confirmed', '16-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (192, 'deaths', '17-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3660, 'recovered', '17-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6870, 'confirmed', '17-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (192, 'deaths', '18-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3758, 'recovered', '18-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6952, 'confirmed', '18-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (193, 'deaths', '19-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (3901, 'recovered', '19-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7023, 'confirmed', '19-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (194, 'deaths', '20-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4098, 'recovered', '20-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7133, 'confirmed', '20-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (196, 'deaths', '21-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4280, 'recovered', '21-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7211, 'confirmed', '21-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (197, 'deaths', '22-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4377, 'recovered', '22-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7332, 'confirmed', '22-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (198, 'deaths', '23-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4638, 'recovered', '23-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7406, 'confirmed', '23-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (199, 'deaths', '24-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4703, 'recovered', '24-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7433, 'confirmed', '24-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (200, 'deaths', '25-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4774, 'recovered', '25-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7532, 'confirmed', '25-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (202, 'deaths', '26-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4881, 'recovered', '26-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7577, 'confirmed', '26-05-2020');


INSERT INTO Cases (Number, Type, Date)
VALUES (202, 'deaths', '27-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (4978, 'recovered', '27-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7601, 'confirmed', '27-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (202, 'deaths', '28-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5195, 'recovered', '28-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7643, 'confirmed', '28-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (202, 'deaths', '29-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5271, 'recovered', '29-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7714, 'confirmed', '29-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (204, 'deaths', '30-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5401, 'recovered', '30-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7780, 'confirmed', '30-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (205, 'deaths', '31-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5459, 'recovered', '31-05-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7807, 'confirmed', '31-05-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (205, 'deaths', '01-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (5893, 'recovered', '01-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7833, 'confirmed', '01-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (206, 'deaths', '02-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6410, 'recovered', '02-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7866, 'confirmed', '02-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (206, 'deaths', '03-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (6866, 'recovered', '03-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7922, 'confirmed', '03-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (208, 'deaths', '04-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7195, 'recovered', '04-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8003, 'confirmed', '04-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (208, 'deaths', '05-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7268, 'recovered', '05-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8071, 'confirmed', '05-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (208, 'deaths', '06-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7315, 'recovered', '06-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8151, 'confirmed', '06-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (208, 'deaths', '07-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7364, 'recovered', '07-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8224, 'confirmed', '07-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (208, 'deaths', '08-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7408, 'recovered', '08-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8302, 'confirmed', '08-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (210, 'deaths', '09-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7493, 'recovered', '09-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8437, 'confirmed', '09-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (211, 'deaths', '10-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7565, 'recovered', '10-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8508, 'confirmed', '10-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (211, 'deaths', '11-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7583, 'recovered', '11-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8537, 'confirmed', '11-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (212, 'deaths', '12-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7618, 'recovered', '12-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8610, 'confirmed', '12-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (212, 'deaths', '13-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7696, 'recovered', '13-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8692, 'confirmed', '13-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (212, 'deaths', '14-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7765, 'recovered', '14-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8793, 'confirmed', '14-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (212, 'deaths', '15-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7828, 'recovered', '15-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8885, 'confirmed', '15-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (212, 'deaths', '16-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7937, 'recovered', '16-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8931, 'confirmed', '16-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (213, 'deaths', '17-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (7993, 'recovered', '17-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8997, 'confirmed', '17-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (213, 'deaths', '18-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8041, 'recovered', '18-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9074, 'confirmed', '18-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (213, 'deaths', '19-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8117, 'recovered', '19-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9613, 'confirmed', '19-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (213, 'deaths', '20-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8223, 'recovered', '20-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9839, 'confirmed', '20-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (214, 'deaths', '21-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8284, 'recovered', '21-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9977, 'confirmed', '21-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (214, 'deaths', '22-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8366, 'recovered', '22-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (10172, 'confirmed', '22-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (214, 'deaths', '23-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8407, 'recovered', '23-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (10344, 'confirmed', '23-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (216, 'deaths', '24-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8468, 'recovered', '24-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (10907, 'confirmed', '24-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (217, 'deaths', '25-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8500, 'recovered', '25-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11338, 'confirmed', '25-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (218, 'deaths', '26-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8656, 'recovered', '26-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11633, 'confirmed', '26-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (220, 'deaths', '27-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8723, 'recovered', '27-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11877, 'confirmed', '27-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (221, 'deaths', '28-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8740, 'recovered', '28-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12052, 'confirmed', '28-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (225, 'deaths', '29-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8833, 'recovered', '29-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12290, 'confirmed', '29-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (228, 'deaths', '30-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (8920, 'recovered', '30-06-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12533, 'confirmed', '30-06-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (228, 'deaths', '01-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9026, 'recovered', '01-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12636, 'confirmed', '01-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (229, 'deaths', '02-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9090, 'recovered', '02-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12969, 'confirmed', '02-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (230, 'deaths', '03-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9160, 'recovered', '03-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13288, 'confirmed', '03-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (232, 'deaths', '04-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9329, 'recovered', '04-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13822, 'confirmed', '04-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (235, 'deaths', '05-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (9725, 'recovered', '05-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14215, 'confirmed', '05-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (237, 'deaths', '06-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (10173, 'recovered', '06-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14379, 'confirmed', '06-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (240, 'deaths', '07-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (10639, 'recovered', '07-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14607, 'confirmed', '07-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (242, 'deaths', '08-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11316, 'recovered', '08-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14771, 'confirmed', '08-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (242, 'deaths', '09-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11447, 'recovered', '09-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15079, 'confirmed', '09-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (243, 'deaths', '10-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (11827, 'recovered', '10-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15328, 'confirmed', '10-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (245, 'deaths', '11-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12065, 'recovered', '11-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15542, 'confirmed', '11-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (250, 'deaths', '12-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12283, 'recovered', '12-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15745, 'confirmed', '12-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (255, 'deaths', '13-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (12934, 'recovered', '13-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15936, 'confirmed', '13-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (257, 'deaths', '14-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13442, 'recovered', '14-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16097, 'confirmed', '14-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (259, 'deaths', '15-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13821, 'recovered', '15-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16262, 'confirmed', '15-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (263, 'deaths', '16-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (13965, 'recovered', '16-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16545, 'confirmed', '16-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (264, 'deaths', '17-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14360, 'recovered', '17-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16726, 'confirmed', '17-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (269, 'deaths', '18-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14620, 'recovered', '18-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17015, 'confirmed', '18-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (273, 'deaths', '19-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (14921, 'recovered', '19-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17236, 'confirmed', '19-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (276, 'deaths', '20-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15132, 'recovered', '20-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17562, 'confirmed', '20-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (280, 'deaths', '21-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15389, 'recovered', '21-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17742, 'confirmed', '21-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (285, 'deaths', '22-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15636, 'recovered', '22-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17962, 'confirmed', '22-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (292, 'deaths', '23-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (15872, 'recovered', '23-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (18264, 'confirmed', '23-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (299, 'deaths', '24-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16100, 'recovered', '24-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (18834, 'confirmed', '24-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (305, 'deaths', '25-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16282, 'recovered', '25-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (19645, 'confirmed', '25-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (313, 'deaths', '26-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16438, 'recovered', '26-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (20278, 'confirmed', '26-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (316, 'deaths', '27-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (16553, 'recovered', '27-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (20887, 'confirmed', '27-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (327, 'deaths', '28-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17066, 'recovered', '28-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (21387, 'confirmed', '28-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (334, 'deaths', '29-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17125, 'recovered', '29-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (22213, 'confirmed', '29-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (346, 'deaths', '30-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17311, 'recovered', '30-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (23259, 'confirmed', '30-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (353, 'deaths', '31-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17658, 'recovered', '31-07-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (24322, 'confirmed', '31-07-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (367, 'deaths', '01-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (17960, 'recovered', '01-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (25015, 'confirmed', '01-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (382, 'deaths', '02-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (18435, 'recovered', '02-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (25537, 'confirmed', '02-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (401, 'deaths', '03-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (18968, 'recovered', '03-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (26196, 'confirmed', '03-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (417, 'deaths', '04-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (19629, 'recovered', '04-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (27217, 'confirmed', '04-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (435, 'deaths', '05-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (19994, 'recovered', '05-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (28500, 'confirmed', '05-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (449, 'deaths', '06-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (20553, 'recovered', '06-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (29644, 'confirmed', '06-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (461, 'deaths', '07-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (21548, 'recovered', '07-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (30662, 'confirmed', '07-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (480, 'deaths', '08-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (22190, 'recovered', '08-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (32007, 'confirmed', '08-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (498, 'deaths', '09-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (23347, 'recovered', '09-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (33237, 'confirmed', '09-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (516, 'deaths', '10-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (24524, 'recovered', '10-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (34063, 'confirmed', '10-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (533, 'deaths', '11-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (25385, 'recovered', '11-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (35195, 'confirmed', '11-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (556, 'deaths', '12-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (25677, 'recovered', '12-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (36694, 'confirmed', '12-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (584, 'deaths', '13-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (26687, 'recovered', '13-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (37935, 'confirmed', '13-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (611, 'deaths', '14-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (27644, 'recovered', '14-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (39241, 'confirmed', '14-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (632, 'deaths', '15-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (28566, 'recovered', '15-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (41017, 'confirmed', '15-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (658, 'deaths', '16-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (29344, 'recovered', '16-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (42489, 'confirmed', '16-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (681, 'deaths', '17-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (29941, 'recovered', '17-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (43558, 'confirmed', '17-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (714, 'deaths', '18-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (31002, 'recovered', '18-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (44803, 'confirmed', '18-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (743, 'deaths', '19-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (31576, 'recovered', '19-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (46313, 'confirmed', '19-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (775, 'deaths', '20-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (32806, 'recovered', '20-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (47638, 'confirmed', '20-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (817, 'deaths', '21-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (34199, 'recovered', '21-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (49247, 'confirmed', '21-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (858, 'deaths', '22-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (35040, 'recovered', '22-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (50812, 'confirmed', '22-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (888, 'deaths', '23-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (36343, 'recovered', '23-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (52349, 'confirmed', '23-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (920, 'deaths', '24-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (37478, 'recovered', '24-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (53252, 'confirmed', '24-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (955, 'deaths', '25-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (38293, 'recovered', '25-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (54528, 'confirmed', '25-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (984, 'deaths', '26-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (40586, 'recovered', '26-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (55864, 'confirmed', '26-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1011, 'deaths', '27-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (41901, 'recovered', '27-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (57085, 'confirmed', '27-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1052, 'deaths', '28-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (43049, 'recovered', '28-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (58489, 'confirmed', '28-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1078, 'deaths', '29-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (44618, 'recovered', '29-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (60056, 'confirmed', '29-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1111, 'deaths', '30-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (46355, 'recovered', '30-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (61399, 'confirmed', '30-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1141, 'deaths', '31-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (47595, 'recovered', '31-08-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (62590, 'confirmed', '31-08-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1184, 'deaths', '01-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (48922, 'recovered', '01-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (63781, 'confirmed', '01-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1216, 'deaths', '02-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (50357, 'recovered', '02-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (65453, 'confirmed', '02-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1253, 'deaths', '03-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (51223, 'recovered', '03-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (66855, 'confirmed', '03-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1292, 'deaths', '04-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (52483, 'recovered', '04-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (68605, 'confirmed', '04-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1329, 'deaths', '05-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (53929, 'recovered', '05-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (70160, 'confirmed', '05-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1361, 'deaths', '06-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (55274, 'recovered', '06-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (72394, 'confirmed', '06-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1394, 'deaths', '07-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (56096, 'recovered', '07-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (73780, 'confirmed', '07-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1427, 'deaths', '08-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (57239, 'recovered', '08-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (75721, 'confirmed', '08-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1453, 'deaths', '09-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (59723, 'recovered', '09-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (77878, 'confirmed', '09-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1491, 'deaths', '10-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (61850, 'recovered', '10-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (79767, 'confirmed', '10-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1524, 'deaths', '11-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (64194, 'recovered', '11-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (82197, 'confirmed', '11-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1553, 'deaths', '12-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (65867, 'recovered', '12-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (84435, 'confirmed', '12-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1578, 'deaths', '13-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (67528, 'recovered', '13-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (86686, 'confirmed', '13-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1614, 'deaths', '14-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (68970, 'recovered', '14-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (88203, 'confirmed', '14-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1648, 'deaths', '15-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (71047, 'recovered', '15-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (90324, 'confirmed', '15-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1686, 'deaths', '16-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (72968, 'recovered', '16-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (92016, 'confirmed', '16-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1714, 'deaths', '17-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (74930, 'recovered', '17-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (94504, 'confirmed', '17-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1755, 'deaths', '18-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (76690, 'recovered', '18-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (97264, 'confirmed', '18-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1795, 'deaths', '19-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (79008, 'recovered', '19-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (99816, 'confirmed', '19-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1830, 'deaths', '20-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (80732, 'recovered', '20-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (101743, 'confirmed', '20-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1855, 'deaths', '21-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (84158, 'recovered', '21-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (103119, 'confirmed', '21-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1889, 'deaths', '22-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (85883, 'recovered', '22-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (105346, 'confirmed', '22-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1918, 'deaths', '23-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (88244, 'recovered', '23-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (107743, 'confirmed', '23-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1956, 'deaths', '24-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (90186, 'recovered', '24-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (110099, 'confirmed', '24-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (1998, 'deaths', '25-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (91932, 'recovered', '25-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (112522, 'confirmed', '25-09-2020');

INSERT INTO Cases (Number, Type, Date)
VALUES (2041, 'deaths', '26-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (94150, 'recovered', '26-09-2020');
INSERT INTO Cases (Number, Type, Date)
VALUES (115241, 'confirmed', '26-09-2020');
