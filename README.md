# MoroccoCovid19API
> Morocco COVID-19 API (cases: Confirmed,Recovered,Deaths) according to the Ministry of Health

![](https://img.shields.io/badge/license-MIT-blue)
![](https://img.shields.io/badge/version-1.0-orange)
![](https://img.shields.io/badge/gson-2.8.6-blue)
![](https://img.shields.io/badge/sparkjava-2.9.0-purple)
![](https://img.shields.io/badge/sqlite_jdbc-3.8.11.2-red)

## Built With
* [gson](https://github.com/google/gson)
* [sparkjava](https://github.com/perwendel/spark)


## Features
* [x] Get daily cases
* [x] Get all cases
* [x] Get all cases by type
* [x] Get the case number by type and date

## How To Use
> Get daily cases according to type :
```shell
curl http://localhost/cases/recovered/daily
```
Example of result :
```json
[  
   {
     "date": "19-05-2020",
     "type": "recovered",
     "number": 143
   },
   {
     "date": "20-05-2020",
     "type": "recovered",
     "number": 197
   },
   {
     "date": "21-05-2020",
     "type": "recovered",
     "number": 182
   },
   {
     "date": "22-05-2020",
     "type": "recovered",
     "number": 97
   },
   {
     "date": "23-05-2020",
     "type": "recovered",
     "number": 261
   },
   {
     "date": "24-05-2020",
     "type": "recovered",
     "number": 65
   },
   {
     "date": "25-05-2020",
     "type": "recovered",
     "number": 71
   },
   {
     "date": "26-05-2020",
     "type": "recovered",
     "number": 67
   }
 ]
  
```

> Get all cases :
```shell
curl http://localhost/cases/
```
Example of result :
```json
[
  {
    "date": "17-05-2020",
    "type": "recovered",
    "number": 3660
  },
  {
    "date": "17-05-2020",
    "type": "confirmed",
    "number": 6870
  },
  {
    "date": "18-05-2020",
    "type": "deaths",
    "number": 192
  },
  {
    "date": "18-05-2020",
    "type": "recovered",
    "number": 3758
  },
  {
    "date": "18-05-2020",
    "type": "confirmed",
    "number": 6952
  }
]
```
> Get all cases by type :
```shell
curl http://localhost/cases/deaths
```
Example of result :
```json
[
  {
    "date": "12-03-2020",
    "type": "deaths",
    "number": 1
  },
  {
    "date": "17-03-2020",
    "type": "deaths",
    "number": 2
  },
  {
    "date": "20-03-2020",
    "type": "deaths",
    "number": 3
  }
]
```
> Get the case number by type and date :
```shell
curl http://localhost/cases/deaths/12/03/2020
```
Example of result :
```json
{
  "date": "12-03-2020",
  "type": "deaths",
  "number": 1
}
```


## Examples
> All Confirmed Cases :
```html
<!DOCTYPE html>
<html>
<head>
    <title>Confirmed Cases</title>
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    <script src="bower_components/chartist/dist/chartist.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
</head>
<body>
<div class="ct-chart ct-perfect-fourth"></div>
<script>
    var the_data = [];
    $.ajax({
        url: "http://localhost/cases/confirmed/",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index) {
                the_data.push({x: moment(data[index].date, "DD-MM-YYYY"), y: data[index].number});
            });
            new Chartist.Line('.ct-chart', {
                series: [
                    {
                        name: 'confirmed',
                        data: the_data
                    }
                ]
            }, {
                axisX: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 20,
                    labelInterpolationFnc: function (value) {
                        return moment(value).format('DD-MM-YYYY');
                    }
                }
            });
        }
    });
</script>
</body>
</html>
```
![](screenshots/screenshot1.png)

> Daily Recovered Cases :
```html
<!DOCTYPE html>
<html>
<head>
    <title>Daily Recovered Cases</title>
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    <script src="bower_components/chartist/dist/chartist.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
</head>
<body>
<div class="ct-chart ct-perfect-fourth"></div>
<script>
    var the_data = [];
    $.ajax({
        url: "http://localhost/cases/recovered/daily",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index) {
                the_data.push({x: moment(data[index].date, "DD-MM-YYYY"), y: data[index].number});
            });
            new Chartist.Line('.ct-chart', {
                series: [
                    {
                        name: 'daily-recovered',
                        data: the_data
                    }
                ]
            }, {
                axisX: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 20,
                    labelInterpolationFnc: function (value) {
                        return moment(value).format('DD-MM-YYYY');
                    }
                }
            });
        }
    });
</script>
</body>
</html>
```
![](screenshots/screenshot2.png)

> Daily Confirmed Cases :
```html
<!DOCTYPE html>
<html>
<head>
    <title>Daily Confirmed Cases</title>
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    <script src="bower_components/chartist/dist/chartist.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
</head>
<body>
<div class="ct-chart ct-perfect-fourth"></div>
<script>
    var the_data = [];
    $.ajax({
        url: "http://localhost/cases/confirmed/daily",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index) {
                the_data.push({x: moment(data[index].date, "DD-MM-YYYY"), y: data[index].number});
            });
            new Chartist.Line('.ct-chart', {
                series: [
                    {
                        name: 'daily-confirmed',
                        data: the_data
                    }
                ]
            }, {
                axisX: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 20,
                    labelInterpolationFnc: function (value) {
                        return moment(value).format('DD-MM-YYYY');
                    }
                }
            });
        }
    });
</script>
</body>
</html>
```
![](screenshots/screenshot3.png)

> Daily Deaths Cases :
```html
<!DOCTYPE html>
<html>
<head>
    <title>Daily Deaths Cases</title>
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">
    <script src="bower_components/chartist/dist/chartist.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
</head>
<body>
<div class="ct-chart ct-perfect-fourth"></div>
<script>
    var the_data = [];
    $.ajax({
        url: "http://localhost/cases/deaths/daily",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index) {
                the_data.push({x: moment(data[index].date, "DD-MM-YYYY"), y: data[index].number});
            });
            new Chartist.Line('.ct-chart', {
                series: [
                    {
                        name: 'daily-deaths',
                        data: the_data
                    }
                ]
            }, {
                axisX: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 20,
                    labelInterpolationFnc: function (value) {
                        return moment(value).format('DD-MM-YYYY');
                    }
                }
            });
        }
    });
</script>
</body>
</html>
```
![](screenshots/screenshot4.png)


## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License
[MIT License](https://choosealicense.com/licenses/mit/)


## Contact
- Yassine Lafryhi - [@YassineLafryhi](https://twitter.com/YassineLafryhi) - [y.lafryhi@gmail.com](mailto:y.lafryhi@gmail.com)
- Project Link : [https://github.com/Yassine-Lafryhi/MoroccoCovid19API](https://github.com/Yassine-Lafryhi/MoroccoCovid19API)
